package streamingResearch.request.picker;

import java.util.List;

public interface Picker<E> {
	
	public E get(List<E> list);
	
	public String getDescription();

}
