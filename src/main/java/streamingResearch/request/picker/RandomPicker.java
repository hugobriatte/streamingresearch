package streamingResearch.request.picker;

import java.util.List;
import java.util.Random;

public class RandomPicker<E> implements Picker<E> {
	
	private static final Random rand = new Random();

	@Override
	public E get(List<E> list) {
		System.out.println(list.size());
		return list.get(rand.nextInt(list.size()));
	}

	@Override
	public String getDescription() {
		return "un au hasard";
	}

}
