package streamingResearch.request.picker;

import java.util.List;

public class FirstPicker<E> implements Picker<E> {

	@Override
	public E get(List<E> list) {
		return list.get(0);
	}

	@Override
	public String getDescription() {
		return "le 1er";
	}

}
