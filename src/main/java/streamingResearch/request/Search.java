package streamingResearch.request;

import java.io.IOException;
import java.util.InputMismatchException;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;
import hugoBriatte.santabot.serverManagment.requestManager.requests.Request;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.ArgumentDescriptor;
import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;
import streamingResearch.animeFormat.AnimeFormatter;
import streamingResearch.dao.MediaDao;
import streamingResearch.dao.MediaMetier;
import streamingResearch.dao.Genre;
import streamingResearch.dao.Type;
import streamingResearch.request.dataType.GenreDataType;
import streamingResearch.request.dataType.TypeDataType;
import streamingResearch.request.picker.Picker;
public class Search extends Request {
	
	protected Picker<MediaMetier> picker;

	public Search(Picker<MediaMetier> picker) {
		super(new ArgumentDescriptor[] {new ArgumentDescriptor("genre", "g", "Genre, cf getAllGenreType", false, true),
										new ArgumentDescriptor("type", "t", "Type de l'oeuvre (Anime ou Manga)", true, true)},
				new DataType<?>[] {new GenreDataType(), new TypeDataType()},
				"Prends une liste d'anime correspondant et choisit " + picker.getDescription());
		this.picker = picker;
	}

	@Override
	public void execute(String arg0, MessageCreateEvent arg1) {
		try {
			ParseRequestResult req = translate(arg1.getServer().get(), arg0);
			Type type = (Type) req.arg.getEntry(true, "t").getValue().getValue();
			MediaMetier anime;
			if(req.arg.isDefined(true, "g")) {
				Genre g = (Genre) req.arg.getEntry(true, "g").getValue().getValue();
				anime = picker.get(new MediaDao().search(req.left, g, type));
			} else {
				anime = picker.get(new MediaDao().search(req.left, type));
			}
			arg1.getChannel().sendMessage(
					new AnimeFormatter().getEmbdedBuilder(anime));
		} catch (Exception e) {
			new MessageBuilder().append("ERREUR").send(arg1.getChannel());
		}
	}

}
