package streamingResearch.request.dataType;

import java.util.InputMismatchException;

import org.javacord.api.entity.server.Server;

import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;
import streamingResearch.dao.Genre;

public class GenreDataType implements DataType<Genre> {

	@Override
	public Genre translate(Server arg0, String arg1) throws InputMismatchException {
		try {
			return Genre.valueOf(arg1.toUpperCase());
		} catch (Exception e) {
			throw new InputMismatchException();
		}
	}

}
