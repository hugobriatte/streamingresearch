package streamingResearch.request.dataType;

import java.util.InputMismatchException;

import org.javacord.api.entity.server.Server;

import hugoBriatte.santabot.serverManagment.requestManager.requests.arguments.dataType.DataType;
import streamingResearch.dao.Type;

public class TypeDataType implements DataType<Type> {

	@Override
	public Type translate(Server arg0, String arg1) throws InputMismatchException {
		switch (arg1.toUpperCase()) {
		case "ANIME":
			return Type.ANIME;
		case "MANGA":
			return Type.MANGA;
		default:
			throw new InputMismatchException();
		}
	}

}
