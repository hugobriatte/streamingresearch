package streamingResearch.request;

import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import hugoBriatte.santabot.serverManagment.requestManager.requests.Request;
import streamingResearch.dao.Genre;

public class GetAllGenre extends Request {

	public GetAllGenre() {
		super("Liste tout les types d'animé reconnu par le bot");
	}

	@Override
	public void execute(String arg0, MessageCreateEvent arg1) {
		MessageBuilder builder = new MessageBuilder();
		for(Genre g : Genre.values()) {
			builder.append(g.name());
			builder.append("\n");
		}
		builder.send(arg1.getChannel());
	}

}
