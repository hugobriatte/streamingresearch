package streamingResearch;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

import hugoBriatte.santabot.serverManagment.requestManager.requests.EchoRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.HelpRequest;
import hugoBriatte.santabot.serverManagment.requestManager.requests.Request;
import streamingResearch.dao.MediaMetier;
import streamingResearch.request.GetAllGenre;
import streamingResearch.request.Search;
import streamingResearch.request.picker.FirstPicker;
import streamingResearch.request.picker.RandomPicker;

public class Main {
	
	public static final String pREFIX = "f!";
	public static final Map<String, Request> requests = new HashMap<String, Request>();

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		DiscordApi api = new DiscordApiBuilder().setToken(args[0]).login().join();
		
		requests.put("echo", new EchoRequest());
		requests.put("research", new Search(new FirstPicker<MediaMetier>()));
		requests.put("random", new Search(new RandomPicker<MediaMetier>()));
		requests.put("allGenres", new GetAllGenre());
		requests.put("help", new HelpRequest(requests));
		
		api.addMessageCreateListener(e -> {
			String[] tab = e.getMessage().getContent().split("\\s+", 2);
			String argList = (tab.length >= 2 ? tab[1] : "");
			try {
				if(tab[0].startsWith(pREFIX)) requests.get(tab[0].replace(pREFIX, "")).execute(argList, e);
			} catch(NullPointerException ee) {
				/* RAS */
			}
		});
	}

}
