package streamingResearch.dao;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.Request;
import okhttp3.Response;
import streamingResearch.http.HttpClient;

public class MediaDao {
	
	protected List<MediaMetier> search(Request request) throws IOException {
		Response httpResponse = HttpClient.getClient().newCall(request).execute();
		JSONObject json = new JSONObject(httpResponse.body().string());
		System.out.println(json.toString());
		JSONArray array = json.getJSONArray("results");
		List<MediaMetier> list = new LinkedList<MediaMetier>();
		for(int i = 0; i < array.length(); i += 1) {
			JSONObject object = array.getJSONObject(i);
			list.add(new MediaMetier(object.getString("title"),
					object.getString("synopsis"),
					object.getString("image_url"),
					object.getString("url"),
					object.getDouble("score")));
		}
		
		return list;
	}
	
	public List<MediaMetier> search(String name, Type type) throws IOException {
		return search(new okhttp3.Request.Builder()
				.url("https://api.jikan.moe/v3/search/" + type.toString().toLowerCase() + "?q=" +name + "&limit=50").build());
	}
	
	public List<MediaMetier> search(String name, Genre genre, Type type) throws IOException {
		return search(new okhttp3.Request.Builder()
				.url("https://api.jikan.moe/v3/search/" + type.toString().toLowerCase() + "?q=" + name + "&genre=" + genre.getNum(type) + "&limit=50").build());
	}
	
	

}
