package streamingResearch.dao;

public class MediaMetier {
	
	protected String title;
	protected String synopsys;
	protected String imageLink;
	protected String url;
	protected double rating;

	public MediaMetier(String title, String synopsys, String imageLink, String url, double rating) {
		super();
		this.title = title;
		this.synopsys = synopsys;
		this.imageLink = imageLink;
		this.url = url;
		this.rating = rating;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getSynopsys() {
		return synopsys;
	}

	public void setSynopsys(String synopsys) {
		this.synopsys = synopsys;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
	
	
}
