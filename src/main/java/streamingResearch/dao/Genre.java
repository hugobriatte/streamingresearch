package streamingResearch.dao;

public enum Genre {
	
	ACTION(1),
	AVENTURE(2),
	VOITURE(3),
	COMEDIE(4),
	DEMONS(6),
	MYSTERE(7),
	DRAMA(8),
	FANTASIE(10),
	HISTORIQUE(13),
	HORREUR(14),
	MAGIQUE(16),
	MECHA(18),
	SAMURAI(21),
	ROMANCE(22),
	ECOLE(23),
	SCI_FI(24),
	ESPACE(29),
	SPORT(30),
	VAMPIRE(32),
	TRANCHE_DE_VIE(36),
	SUPERNATUREL(37),
	POLICE(39),
	PSYCHOLOGIQUE(40),
	SEINEN(42, 41),
	JOSEI(43, 42),
	THRILLER(41, 45);
	
	private int numAnime;
	private int numManga;
	
	private Genre(int num) {
		this(num, num);
	}

	private Genre(int numAnime, int numManga) {
		this.numAnime = numAnime;
		this.numManga = numManga;
	}

	public int getNumAnime() {
		return numAnime;
	}

	public int getNumManga() {
		return numManga;
	}
	
	public int getNum(Type type) {
		if(type.equals(Type.ANIME)) return getNumAnime();
		else return getNumManga();
	}

}
