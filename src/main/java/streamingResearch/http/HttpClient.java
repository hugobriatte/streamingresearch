package streamingResearch.http;

import okhttp3.OkHttpClient;

public class HttpClient {
	
	protected static final HttpClient INSTANCE;
	protected OkHttpClient client;
	
	static {
		INSTANCE = new HttpClient();
	}
	
	private HttpClient() {
		this.client = new OkHttpClient();
	}
	
	public static OkHttpClient getClient() {
		return INSTANCE.client;
	}

}
