package streamingResearch.animeFormat;

import org.javacord.api.entity.message.embed.EmbedBuilder;

import streamingResearch.dao.MediaMetier;

public class AnimeFormatter {
	
	public EmbedBuilder getEmbdedBuilder(MediaMetier metier) {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(metier.getTitle());
		builder.addField("Resumé", metier.getSynopsys());
		builder.setUrl(metier.getUrl());
		builder.setImage(metier.getImageLink());
		return builder;
	}

}
