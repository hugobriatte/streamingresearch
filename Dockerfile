#CONSTRUCTION
FROM maven:3.3.9-jdk-8 AS builder
COPY src/ /tmp/src/
COPY lib/ /tmp/lib/
COPY pom.xml /tmp/
WORKDIR /tmp/
RUN mvn clean compile dependency:copy-dependencies

#PREPARATION DU RUNNER
FROM openjdk:8 AS runner
WORKDIR /release/
COPY --from=builder /tmp/lib/* ./dependency/
COPY --from=builder /tmp/target/dependency/* ./dependency/
COPY --from=builder /tmp/target/classes/ ./classes/
WORKDIR ./classes/
RUN ls ../dependency/
CMD ["DEFAULT_TOKEN"]
ENTRYPOINT ["java", "-classpath", "../dependency/*:.", "streamingResearch.Main"]
